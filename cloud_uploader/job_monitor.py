#!/usr/bin/env python
core_connect_path = "/var/VFXServerWSDebug/scripts/"

import logging
import sys
sys.path.append(core_connect_path)

from CoreConnect import CoreConnect
from Asset import Asset

from CloudJob import CloudJob, cloud_job_from_json
from shutil import move

job_runner_path = core_connect_path+"/cloud_uploader/cloud_job_runner.py"
cj_path = "/mnt/VFXRepositoryDebug/CloudJobs/"

from glob import glob
from subprocess import Popen
from json import load

import os

from time import sleep

"""
Monitor the cj_path, search for jobs not being currently processed and run them
using cloud_job_runner.py
"""
MAX_ACTIVE_JOBS = 5

LOCK_FILE = cj_path+"WATCHER.LOCK"

#In megs
MAX_SIZE_FOR_ACTIVE_JOBS = 1024 * 20

#Cloudfrount doesn't allow files larger than 20G
#In bytes
MAX_JOB_SIZE = 1024 * 1024 * 1024 * 20


def get_job_size(job_file):
    cj = None
    size = 0
    
    #Load the cloud job object from the file
    try:
        with open(job_file, 'r') as fh:
            cj = cloud_job_from_json( load(fh) )
        
        if cj is None:
            print "*Error while loading from: "+g
            size = -1
        else:
            size = cj.asset.asset_size
    except Exception, e:
        size = -1
        print "*Exception in get_job_size: "+e.__str__()
    finally:
        return size


def get_size_of_active_jobs():
    #In bytes
    total_size = 0
    globname = cj_path+"*.cj"
    
    for g in glob(globname):
        if not os.path.exists(g+".failed"):
            #print "Getting size for: " +g
            s = get_job_size(g)
            if s > -1:
                total_size += s
    return total_size
    

if __name__ == "__main__":
    #Prevent running more than one instance
    print "LOCKFILE: " + LOCK_FILE
    if os.path.exists(LOCK_FILE):
        print "Lock file found, exiting"
        exit(0)
    
    with open(LOCK_FILE, 'w') as fh:
        os.fsync(fh)
    
    user = "CLOUDUSER"
    password = "THEPASSWD"
    
    log_filename = core_connect_path+"/cloud_uploader/log.txt"
    logger = logging.getLogger("cloud_uploader_logger")
    hdlr = logging.FileHandler(log_filename)
    formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s")
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.DEBUG)
    
    cc_path = "/var/VFXServerWSDebug/scripts/cloud_uploader/"
    cc = CoreConnect(user=user, logger="cloud_uploader_logger", core_connect_path = cc_path)
    cc.port = "61001"
    
    total_size = get_size_of_active_jobs()
    print "Size of jobs %.2f MB " % (total_size / 1048576.0)
    
    #Load pending jobs
    pending_jobs = cc.load_pending_cloud_jobs(user, password)
    
    print "# of pending jobs: "+str(len(pending_jobs))
    #Write using the number of running jobs as limit
    #Make sure there's only one job for that asset, doing that action and hasn't failed
    job_count = 0
    upload_globname = cj_path+"*-upload-*.cj"
    for pu in pending_jobs:
        job_count = len(glob( upload_globname ))
        failed_count = len(glob( upload_globname+".failed"))
        done_count = len(glob( upload_globname+".done"))
        success_count = len(glob( upload_globname+".success"))
        
        actual_count = job_count - failed_count - success_count
        print "# of active jobs: "+str(actual_count)
        
        if actual_count >= MAX_ACTIVE_JOBS:
            print "MAX_ACTIVE_JOBS reached, won't write new ones until some are done"
            break
        
        
        cj = CloudJob(pu)
        
        if pu.cloud_status == pu.CLOUD_STATUS_PENDING_UPLOAD:
            cj.action = "upload"
        elif pu.cloud_status == pu.CLOUD_STATUS_PENDING_DELETE:
            cj.action = "delete"
        elif pu.cloud_status == pu.CLOUD_STATUS_PENDING_LOCALIZE:
            cj.action = "localize"
            
        if cj.asset.asset_size > MAX_JOB_SIZE:
            print "SIZE OF ASSET: " +str(cj.asset.asset_size)
            print "Size exceeded"
            continue
        
        #Check if there's already a job for this asset
        existing_job_glob = cj_path+"*-"+cj.action+"-*-"+str(cj.asset.asset_id)+".cj"
        print "Checking for an existing job with glob: " +existing_job_glob
        if len( glob(existing_job_glob) ) > 0:
            print "Skipping an already existing job\n"
            continue
            
        #print "Status: " pu.cloud_status
        print "Writing a : "+cj.action+" job "
        cj.write_job()
        
    
    #The assets will be stored here to update their status
    assets = []
    globname = cj_path+"*.cj"
    for g in glob(globname):
        cj = None
        
        #Load the cloud job object from the file
        with open(g, 'r') as fh:
            cj = cloud_job_from_json( load(fh) )
        
        if cj is None:
            print "*Error while loading from: "+g
            continue
        
        #Run any idle job
        if not os.path.exists(g+".processing") and not os.path.exists(g+".done") and not os.path.exists(g+".failed"):
            if cj.action == "upload":
                cj.asset.cloud_status = cj.asset.CLOUD_STATUS_UPLOADING
            elif cj.action == "delete":
                cj.asset.cloud_status = cj.asset.CLOUD_STATUS_DELETING
            elif cj.action == "localize":
                cj.asset.cloud_status = cj.asset.CLOUD_STATUS_LOCALIZING
            
            cj.update_job()
            assets.append(cj.asset)
            
            print "Running job: " + g
            p = Popen(["python", job_runner_path, cj.filename], shell=False)
            p.wait()
            
        #Remove done jobs
        #This verification is probably overkill, but since we're dealing with files
        #it's possible that the SO hasn't finished writing to them
        if os.path.exists(g+".done") and os.path.exists(g+".success") and not os.path.exists(g+".failed"):
            print "Done job: " +g
            if cj.action  == "upload":
                cj.asset.cloud_status = cj.asset.CLOUD_STATUS_UPLOAD_DONE
            elif cj.action == "delete":
                cj.asset.cloud_status = cj.asset.CLOUD_STATUS_DELETE_DONE
            elif cj.action == "localize":
                cj.asset.cloud_status = cj.asset.CLOUD_STATUS_LOCALIZE_DONE
            
            assets.append(cj.asset)
            
        #Mark failed jobs as such
        if os.path.exists(g+".failed"):
            if cj.action == "upload":
                cj.asset.cloud_status = cj.asset.CLOUD_STATUS_UPLOAD_FAILED
            elif cj.action == "delete":
                cj.asset.cloud_status = cj.asset.CLOUD_STATUS_DELETE_FAILED
            elif cj.action == "localize":
                cj.asset.cloud_status = cj.asset.CLOUD_STATUS_LOCALIZE_FAILED
            
            assets.append(cj.asset)
            
        
    #Update the assets status
    print "LEN ASSETS: " + str(len(assets))
    if len(assets) > 0 :
        print "Updating statusessss..."
        cc.update_cloud_jobs(user, password, assets)
    else:
        print "Nothing to update"
    
    
    #Remove done and failed jobs
    for g in glob(globname):
        if os.path.exists(g+".done") and os.path.exists(g+".success") and not os.path.exists(g+".failed"):
            try:
                move(g, cj_path+"DoneJobs/"+os.path.basename(g))
                os.remove(g+".done")
                os.remove(g+".processing")
                os.remove(g+".success")
                pass
            except OSError, e:
                print "*Error while removing: " +g+", "+e.__str__()
                break
        if os.path.exists(g+".failed"):
            try:
                move(g, cj_path+"FailedJobs/"+os.path.basename(g))
                move(g+".failed", cj_path+"FailedJobs/"+os.path.basename(g+".failed"))
                os.remove(g+".done")
                os.remove(g+".processing")
                pass
            except OSError, e:
                print "*Error while removing: " +g+", "+e.__str__()
                break
        
        
    
    #Remove the lock file
    try:
        os.remove(LOCK_FILE)
        exit(0)
    except OSError, e:
        print "*Could not remove lock file"
        exit(1)
    
    

