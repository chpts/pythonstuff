
base_path = "/var/VFXServerWSDebug/scripts/"
import sys
sys.path.append(base_path)

from cloud_utils.cloud_utils import sign_restricted_url

#Returns a signed url to the asset on the cloud which is only accessible by the requesting ip
if __name__ == "__main__":
    if len(sys.argv) != 6:
        print "Usage pyton url_signer.py key_id, key_file.csv ip bucket_url bucket_file_path"
        exit(0)
    
    key_id = sys.argv[1]
    key_file = sys.argv[2]
    ip = sys.argv[3]
    bucket_url = sys.argv[4]
    bucket_file_path = sys.argv[5]
    
    #Check if the request was made from inside ollin
    #If so, ignore the ip
    ignore_ip = False
    
    if "172.16.28." in ip or "172.16.29." in ip or ip =="localhost":
        ignore_ip = True
    
    print sign_restricted_url(bucket_url, bucket_file_path, sourceIP=ip, cloudKeyPairID=key_id, cloudKeyFile=key_file, ignoreIP=ignore_ip)
    exit(0)



