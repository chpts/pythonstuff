#!/usr/bin/env python
import gevent.monkey
gevent.monkey.patch_all()


import sys
import os

base_path = "/var/VFXServerWSDebug/scripts/"
sys.path.append(base_path)

repo_path = "/mnt/VFXRepositoryDebug/"

key_file = base_path+"AWS_keys/rootkey.csv"

from cloud_uploader.CloudJob import CloudJob, cloud_job_from_json
from cloud_utils.cloud_utils import CloudHelperAWS

from cloud_downloader.cloud_downloader import download_asset

from json import load



if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: python cloud_job_runner.py cloud_job_file.cj"
        exit(0)
    
    job_file = sys.argv[1]
    
    pid = os.fork()
    
    if pid > 0:
        exit(0)
    
    #Mark the job as being processed
    if not os.path.exists(job_file+".processing"):
        with open(job_file+".processing", 'w') as fh:
            pass
    
    cj = CloudJob()
    
    #Load the job into the cj object
    with open(job_file, 'r') as fh:
        cj = cloud_job_from_json( load(fh) )
    
    
    #Open the rootkey file and get our key/secret pair
    key_id = ""
    secret = ""
    with open(key_file, 'r') as fh:
        pairs = fh.readlines()
    
    if len(pairs) != 2:
        print "*Could not get the key/secret pair from : "+key_file
        exit(1)
    
    key_id = pairs[0].strip()
    secret = pairs[1].strip()
    
    key_id = key_id.replace("AWSAccessKeyId=", "")
    secret = secret.replace("AWSSecretKey=", "")
    
    #Initialize the cloud helper
    ch = CloudHelperAWS(key_id, secret)
    error = ch.initialize("ollinvfxcore")
    
    last_error_string = ch.lastErrorString
    if error == 0:
        bucket_asset_name = "assets/"+str(cj.asset.project_id)+"/"+cj.asset.asset_name
        if cj.action == "upload":
            if not os.path.exists(cj.asset.path):
                print "*Error in cloud_job_runner, asset: " +cj.asset.path+" does not exist!"
                print "*Error while processing cloud job: "+ ch.lastErrorString
                
                with open(job_file+".failed", 'w') as fh:
                    fh.write("Asset: " +cj.asset.path+" does not exist")
                    os.fsync(fh)
                
                exit(1)
            error = ch.uploadFileToCloud(cj.asset.path, bucket_asset_name)
            last_error_string = ch.lastErrorString
        elif cj.action == "delete":
            error = ch.deleteFileFromCloud(bucket_asset_name)
            last_error_string = ch.lastErrorString
        elif cj.action == "localize":
            #Localize the file
            download_path = repo_path+"assets/"+str(cj.asset.project_id)+"/"
            
            error = download_asset(cj.asset.project_id, cj.asset.asset_name, download_path)
            last_error_string = ch.lastErrorString
            print "wget returns with: "+str(error)
            if error == 0:
                print "Deleting file: "+bucket_asset_name
                #If download was succesfull, set its status to oncloud=False
                #And delete the file from the cloud
                error = ch.deleteFileFromCloud(bucket_asset_name)
                print "Done with error: " +str(error)
                last_error_string = ch.lastErrorString
                print "and lastErrorString: "+last_error_string
    
    
    #Mark the job as failed if applies
    if error == 0:
        with open(job_file+".success", 'w') as fh:
            os.fsync(fh)
    else:
        print "*Error while processing job: "+ str(error)
        print "*Error string: "+ str(last_error_string)
        with open(job_file+".failed", 'w') as fh:
            fh.write(last_error_string)
    
    #Mark the job as done
    with open(job_file+".done", 'w') as fh:
        os.fsync(fh)
    
    
    
    exit(error)



