
core_connect_path = "/var/VFXServerWSDebug/scripts/"

import logging
import sys
sys.path.append(core_connect_path)

from Asset import Asset, AssetJSONEncoder, asset_from_json

from datetime import datetime

cj_path = "/mnt/VFXRepositoryDebug/CloudJobs/"

import random
import os

from json import JSONEncoder, dumps, load

from cloud_utils import cloud_utils

class CloudJob:
    def __init__(self, asset=Asset()):
        self.asset = asset
        self.filename = ""
        #Action can be upload, download or localize
        self.action = "upload"
        random.seed()
    
    
    def write_job(self):
        now = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        
        self.filename = cj_path+now+"-"+str(random.randrange(0, 99999))+"-"+\
                self.asset.asset_uploader+"-"+self.action+"-"+str(self.asset.project_id)+"-"+str(self.asset.asset_id)+".cj"
        
        
        self.update_job()
        return True
    
    def update_job(self):
        with open(self.filename, 'w') as fh:
            fh.write( dumps(self, cls=CloudJobJSONEncoder, sort_keys=True, indent=4) )
            os.fsync(fh)
    
from ast import literal_eval

def cloud_job_from_json(cj_dict={}):
    cj = CloudJob()
    d = {}
    for k,v in cj_dict.iteritems():
        if k == "asset":
            d[k] = asset_from_json(v)
        else:
            d[k] = v
    
    cj.__dict__ = d
    return cj



class CloudJobJSONEncoder(JSONEncoder):
    def default(self, cj=CloudJob()):
        d = {}
        for k,v in cj.__dict__.iteritems():
            if k == "asset":
                d[k] = AssetJSONEncoder().default(v)
            else:
                d[k] = v
        return d


