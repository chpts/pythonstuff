#!/usr/bin/env python

import os

import sys
base_path = "/var/VFXServerWSDebug/scripts/"
sys.path.append(base_path)

from cloud_utils.cloud_utils import CloudHelperAWS

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "Usage python manual_uploader.py project_id asset"
        exit(0)
    
    project_id = sys.argv[1]
    asset = sys.argv[2]
    
    if not os.path.exists(asset):
        print "*Error: asset "+asset+" does not exist!"
        exit(1)
    
    key_id = ""
    secret = ""
    
    key_file = base_path+"AWS_keys/rootkey.csv"
    with open(key_file, 'r') as fh:
        pairs = fh.readlines()
    
    if len(pairs) != 2:
        print "*Could not get the key/secret pair from : "+key_file
        exit(1)
    
    key_id = pairs[0].strip()
    secret = pairs[1].strip()
    
    key_id = key_id.replace("AWSAccessKeyId=", "")
    secret = secret.replace("AWSSecretKey=", "")
    
    #Initialize the cloud helper
    ch = CloudHelperAWS(key_id, secret)
    error = ch.initialize("ollinvfxcore")
    
    asset_name = os.path.basename(asset)
    bucket_asset_name = "assets/"+project_id+"/"+asset_name
    
    print "Uploading...."
    error = ch.uploadFileToCloud(asset, bucket_asset_name)
    if error != 0:
        print "*Error: "+str(error)+", "+ch.lastErrorString
    else:
        print "Done uploading"
    
    exit(error)
    
