from oauth2client.file import Storage
import oauth2client.client

import httplib2
from  apiclient.discovery import build
import apiclient.http

from subprocess import Popen
from datetime import datetime

class DriveSupport:
    def __init__(self, logger = None, client_secrets="client_secrets.json"):
        self.scope = "https://www.googleapis.com/auth/drive"
        self.key = None
        self.client_secrets = client_secrets
        
        self.set_logger(logger)
        pass
    
    
    def set_logger(self, logger):
        self.logger = logger
    
    
    def get_drive_service(self):
        http = httplib2.Http()
        storage = Storage("credentials.dat")
        credentials = storage.get()
        
        # Save the credentials in storage to be used in subsequent runs.
        if credentials is None or credentials.invalid or True:
            self.logger.warning("Invalid credentials")
            
            print "%s, %s" % (self.client_secrets, self.scope)
            flow = oauth2client.client.flow_from_clientsecrets(self.client_secrets, self.scope)
            flow.redirect_uri = "urn:ietf:wg:oauth:2.0:oob"
            
            auth_url = flow.step1_get_authorize_url()
            
            #Open firefox with the authorization url, wait for user
            p = Popen("firefox '%s'" % auth_url, shell=True)
            
            verification = raw_input("Verification code: ").strip()
            
            credentials = flow.step2_exchange(verification)
            
            storage.put(credentials)
        else:
            credentials.refresh(http)
        
        http = credentials.authorize(http)
        
        drive_service = build("drive", "v2", http=http)
        
        return http, drive_service
    
    
    def get_file_info(self, drive_service, filename):
        items = drive_service.files().list(q="title='"+filename+"'").execute()
        
        if 'items' not in items:
            return None
        
        
        if len(items['items']) == 0:
            print self.logger.critical("No file named: " + filename )
            return None
            
        return items['items'][0]
    
    
    def download(self, filename):
        http, drive_service = self.get_drive_service()
        
        file_info = self.get_file_info(drive_service, filename)
        
        if file_info is None:
            self.logger.critical("Could not retrieve: "+filename)
            return 500, ""
        
        file_id = file_info['id']
        
        the_file = drive_service.files().get(fileId=file_id).execute()
        
        response, content = http.request(the_file['downloadUrl'])
        
        logger.info("Got a %i from the server" % response.status)
        
        if response.status == 200:
            local_file_path = os.path.join( os.path.dirname(__file__), the_file['title'] )
            logger.info("Writing local file %s " % local_file_path)
            
            with open(local_file_path, 'wb') as fh:
                fh.write( content )
        
        return response.status, local_file_path
    
    
    def upload(self, filepath, replace=False):
        filename = os.path.basename(filepath)
        
        http, drive_service = self.get_drive_service()
        
        file = None
        
        if replace:
            file_info = self.get_file_info(drive_service, filename)
            
            if file_info is None:
                self.logger.critical("Could not replace file: "+filepath)
                return False
            
            file_id = file_info['id']
            the_file = drive_service.files().get(fileId=file_id).execute()
            
            now = datetime.now().strftime("%Y-%m-%d_%H_%M_%S")
            fn, extension = os.path.splitext(filename)
            #the_file['title'] = fn+"."+now+extension
            the_file['title'] = file_info['title']
            the_file['mimeType'] = file_info['mimeType']
            mfu = apiclient.http.MediaFileUpload(filepath, file_info['mimeType'], resumable=True)
            file = drive_service.files().update(fileId=file_id, newRevision=True, body=the_file, media_body=mfu).execute()
        else:
            the_file = {'title':filename, 'mimeType': 'application/octet-stream'}
            mfu = apiclient.http.MediaFileUpload(filepath, resumable=True)
            file = drive_service.files().insert(body=the_file, media_body=mfu).execute()
            
            
        return file


if __name__ == "__main__":
    import logging
    import os
    
    curr_path = os.path.dirname("__file__")
    log_filename = os.path.join(curr_path, datetime.now().strftime("applauncher-%Y-%m-%d_%H-%M-%S.log"))
    
    logger = logging.getLogger("AppLauncherLogger")
    hdlr = logging.FileHandler(log_filename)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.DEBUG)
    
    client_secrets = "/home/juan/Projects/pythonstuff/KeepassDrive/client_secrets.json"
    ds = DriveSupport(logger, client_secrets)
    ds.download("Database.kdbx")
    #ds.upload("/home/juan/Projects/CloudStuff/KeepassDrive/jefe_audit.py", replace=True)
    

