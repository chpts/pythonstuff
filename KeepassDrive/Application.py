import os
import time
from hashlib import sha1
from json import load, dumps, JSONEncoder
from subprocess import Popen

class AppEncoder(JSONEncoder):
    serialize_names = frozenset(
            ['name', 'bin', 'args', 'filename'] )
    def default(self, app):
        d = {}
        for k,v in app.__dict__.iteritems():
            if k in self.serialize_names:
                d[k] = v
        return d


class Application:
    def __init__(self, name="", bin="", args=None, filename="", logger=None, config_filename=""):
        self.name = name
        self.bin = bin
        self.args = args if args else []
        self.filename = filename
        self.set_logger(logger)
        self.set_config_file(config_filename)
        self.hash_pre = ""
        self.hash_post = ""
        self.mtime_pre = None
        self.mtime_post = None
    
    
    def set_logger(self, logger):
        self.logger = logger
    
    
    def set_config_file(self, filename=""):
        self.config_file = filename
    
    
    def to_json(self):
        dump = dumps(self, cls=AppEncoder, sort_keys=True, indent=4)
        try:
            with open(self.config_file, 'wb') as cf:
                cf.write(dumps(self, cls=AppEncoder, sort_keys=True, indent=4))
        except Exception, e:
            self.logger.critical("Could not dump app to config file: "+e.__str__())
        
    
    def from_json(self):
        try:
            with open(self.config_file, 'rb') as cf:
                self.__dict__ = load( cf )
        except Exception, e:
            self.logger.critical("Could not load app config from file: "+e.__str__())
    
    
    def run(self):
        error = ""
        
        if not os.path.exists(self.bin):
            error = "Application %s does not exist! " % self.bin
            self.logger.critical(error)
            return -1, error
        
        if not os.access(self.bin, os.X_OK):
            error = "No execution priviledges on %s " % self.bin
            self.logger.critical(error)
            return -1, error
        
        cmd = self.bin+" ".join(self.args)+" "+self.filename
        
        self.logger.info("Running %s " % cmd)
        
        retcode = None
        
        #Wait does not function with every program, nautilus for instance
        self.hash_pre = self.hash()
        
        self.mtime_pre = time.ctime(os.path.getmtime(self.filename))
        print "MTIME PRE: %s " % self.mtime_pre
        p = Popen(cmd, shell=True)
        p.wait()
        retcode = p.poll()
        
        self.logger.info("Done")
        self.hash_post = self.hash()
        self.mtime_post = time.ctime(os.path.getmtime(self.filename))
        print "MTIME POST: %s " % self.mtime_post
        
        return retcode, "Done"
    
    
    def hash(self):
        with open(self.filename, 'rb') as f:
            data = f.read()
            s = sha1()
            s.update("blob %u\0" % len(data))
            s.update(data)
            return s.hexdigest()
    
    
    def file_changed(self):
        if self.mtime_pre is not None and self.mtime_post is not None:
            return self.mtime_pre != self.mtime_post
        return True


if __name__ == "__main__":
    from datetime import datetime
    import logging
    curr_path = os.path.dirname("__file__")
    log_filename = os.path.join(curr_path, datetime.now().strftime("applauncher-%Y-%m-%d_%H-%M-%S.log"))
    
    logger = logging.getLogger("AppLauncherLogger")
    hdlr = logging.FileHandler(log_filename)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.DEBUG)
    
    config_file = '{}'.format(
            os.path.join(os.path.expanduser('~'), 'Projects/pythonstuff/KeepassDrive/keepass.cfg'))
    print('Using config file {}'.format(config_file))
    app = Application("keepass", "/Applications/KeePassX.App/Contents/MacOS/KeePassX", [], "/Users/juan/Documents/Database.kdb", logger, config_file)
    
    app.run()
    
    print("File Changed" if app.file_changed() else "No changes")
    
    app.to_json()
    
    #app = Application()
    #app.set_config_file(config_file)
    
    #app = app.from_json()


