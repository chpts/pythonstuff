import inspect
import logging
import os
from Application import Application
from DriveSupport import DriveSupport
from datetime import datetime
from subprocess import Popen

class AppLauncher:
    def __init__(self, app=Application(), drive=DriveSupport() ):
        self.app = app
        self.drive = drive
        
        # __file__
        curr_path = os.path.dirname(inspect.getfile(inspect.currentframe()))
        
        self.log_filename = os.path.join(curr_path, datetime.now().strftime("applauncher-%Y-%m-%d_%H-%M-%S.log"))
        
        self.logger = logging.getLogger("applauncherlogger")
        hdlr = logging.FileHandler(self.log_filename)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        self.logger.addHandler(hdlr)
        self.logger.setLevel(logging.DEBUG)

        self.app.set_logger(self.logger)
        self.drive.set_logger(self.logger)


    def authenticate(self):
        pass


    def download(self):
        ds = DriveSupport(self.logger)
        pass


    def upload(self):
        pass 



    def cleanup(self):
        try:
            os.remove(self.app.filename)
        except OSError, e:
            self.logger.error("*Error while removing: "+e.__str__())



    def go(self):
        if not self.authenticate():
            pass
        
        response, self.app.filename = self.download()
        if response == 200 and self.app.filename != "":
            pass
            



