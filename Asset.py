#!/usr/bin/env python


class Asset:
    
    def __init__(self):
        self.ON_CLOUD_NO = 0
        self.ON_CLOUD_YES = 1
        
        self.CLOUD_STATUS_WONT_UPLOAD= 0
        self.CLOUD_STATUS_PENDING_UPLOAD = 1
        self.CLOUD_STATUS_UPLOAD_FAILED = -1
        self.CLOUD_STATUS_UPLOADING = 2
        self.CLOUD_STATUS_UPLOAD_DONE = 3
        
        self.CLOUD_STATUS_PENDING_DELETE = 4
        self.CLOUD_STATUS_DELETING = 5
        self.CLOUD_STATUS_DELETE_DONE = 6
        self.CLOUD_STATUS_DELETE_FAILED = -2
        
        self.CLOUD_STATUS_PENDING_LOCALIZE = 7
        self.CLOUD_STATUS_LOCALIZING = 8
        self.CLOUD_STATUS_LOCALIZE_DONE = 9
        self.CLOUD_STATUS_LOCALIZE_FAILED = -3
        
        self.asset_id = -1
        self.asset_name = ""
        self.asset_last_modification = ""
        self.asset_notes = ""
        self.asset_public = 0
        self.asset_size = 0
        self.asset_type = 0 
        self.asset_uploader = ""
        self.asset_upload_date = ""
        self.parent_id = 0
        self.shot_id = -1
        self.project_id = -1
        self.on_cloud = False
        self.cloud_status = self.CLOUD_STATUS_WONT_UPLOAD
        self.path = ""
        self.repo_path = ""


def asset_from_json(asset_dict={}):
    a = Asset()
    a.__dict__ = asset_dict
    return a


from json import JSONEncoder

class AssetJSONEncoder(JSONEncoder):
    def default(self, asset):
        return asset.__dict__
