# -*- coding: utf-8 -*-
#!/usr/bin/env python

#import cloudfiles


import gevent.monkey
#gevent.monkey.patch_all()

import hashlib
import base64
import os
import time
from M2Crypto import RSA
import json
import datetime
import shutil
from StringIO import StringIO

import traceback

from math import ceil
from multiprocessing.dummy import Pool
import time

import logging

logger = logging.getLogger("castingssite")

import boto
from boto.s3.connection import S3Connection
from boto.s3.key import Key
from boto.s3.connection import OrdinaryCallingFormat
import re

import gevent
import gevent.pool
import md5




USE_SOURCEIP_TO_SIGN_CLOUD=True
CLOUD_SIGNATURE_IP = "SOMEIP"

CHUNK_SIZE = 50*1024*1024
UPLOAD_CONCURRENCY = 8
HASH_CHUNK_SIZE=CHUNK_SIZE/2
MAX_PART_RETRIES=3

def upload_part(mp, fname, idx, offset, totalParts, chunkSize=10*1024*1024, AWSS3_API_KEYID="", AWSS3_API_ACCESSKEY="", logFile=None, skipUpload=False):
    lastError=""
    fSize= os.path.getsize(fname)
    f = open(fname)
    
    #f.seek(offset)
    #content = f.read(CHUNK_SIZE)
    
    
    
    success = False
    for x in xrange(MAX_PART_RETRIES):
        try:
            lastError=""
            start=time.time()
            line="PART %i/%i Processing (attempt %i) of %s to bucket %s"%(idx, totalParts, x, fname, mp.bucket_name)
            print line
            if logFile:
                logFile.write(line+"\n")
                logFile.flush()
            f.seek(offset)
            
            #how many hash chunks?
            
            HASH_CHUNK_SIZE=20*1024*1024
            leftToRead=fSize-offset
            if leftToRead>chunkSize:
                leftToRead=chunkSize
            
            
            hash_chunk_num=leftToRead/int(HASH_CHUNK_SIZE)
            rest=leftToRead % HASH_CHUNK_SIZE
            line="PART %i/%i hashing %ix%ib chunks and %i remainder"%(idx, totalParts, hash_chunk_num, HASH_CHUNK_SIZE, rest)
            print line
            if logFile:
                logFile.write(line+"\n")
                logFile.flush()
            
            hasher=md5.new()
            for i in xrange(hash_chunk_num):
                hasher.update(f.read(HASH_CHUNK_SIZE))
                
            if rest>0:
                hasher.update(f.read(rest))
                
            
            chunkMD5=base64.b64encode(hasher.digest())
            chunkMD5Hex=hasher.hexdigest()
            line="PART %i/%i  MD5: %s/%s"%(idx, totalParts, chunkMD5, chunkMD5Hex)
            print line
            if logFile:
                logFile.write(line+"\n")
                logFile.flush()
                
            f.seek(offset)
            if not skipUpload:
                conn=S3Connection(AWSS3_API_KEYID, AWSS3_API_ACCESSKEY)
                bucket = conn.get_bucket(mp.bucket_name)
                p = boto.s3.multipart.MultiPartUpload(bucket)
                p.id = mp.id
                
                p.key_name = mp.key_name
                
                line="PART %i/%i: Uploading %iMB"%(idx, totalParts, leftToRead/1024/1024)
                print line
                if logFile:
                    logFile.write(line+"\n")
                    logFile.flush()
                
                #FAKE MD5 for debugging
                #chunkMD5="DSu5lFaIxT9Ouep5VpM3+g=="
                #chunkMD5Hex="0d2bb9945688c53f4eb9ea79569337fa"
                
                r=p.upload_part_from_file(f, idx+1, replace=True, size = leftToRead, md5=(chunkMD5Hex, chunkMD5))
                print "PART %i/%i: etag: %s"%(idx, totalParts,r.etag)
                #p.upload_part_from_file(StringIO(content), idx+1, replace=True)
                success = True
                
                
                elapsed = time.time()-start
                line="PART %i/%i: %iMB uploaded in %fs (%fMB/s)"%(idx, totalParts, leftToRead/1024/1024, elapsed, leftToRead/1024/1024/elapsed)
                print line
                if logFile:
                    logFile.write(line+"\n")
                    logFile.flush()
                
                
                break
            else:
                success = True
                break
                    
                
        except Exception, e:
            line = "Error in part upload - %s %s %s: %s" % (fname, idx, offset, unicode(e.__str__()))
            lastError=line
            success = False
            print line
            if logFile:
                logFile.write(line+"\n")
                logFile.flush()
            
    
    f.close()
    
    assert success, lastError



class CloudHelperAWS:
    def __init__(self, AWSS3_API_KEYID="", AWSS3_API_ACCESSKEY=""):
        self.conn=None
        self.lastError=0
        self.lastErrorString=""
        self.bucket=None
        self.bucketName=""
        self.AWSS3_API_KEYID = AWSS3_API_KEYID
        self.AWSS3_API_ACCESSKEY = AWSS3_API_ACCESSKEY
        self.uploadedMD5=""
    
    def initialize(self, theBucket, forceNewConnection=False):
        
        #just debugging, to facilitate test don't upload to cloud.
        #self.lastError=0
        #self.lastErrorString=""
        #return self.lastError
        
        try:
            if self.conn == None or forceNewConnection:
                self.conn=S3Connection(self.AWSS3_API_KEYID, self.AWSS3_API_ACCESSKEY)
                self.bucket=self.conn.get_bucket(theBucket)
                self.bucketName=self.bucket.name
            
            self.lastError=0
            self.lastErrorString=""
            return self.lastError
            
        except Exception, inst:
            self.lastError=1
            self.lastErrorString="Error while connecting: "+unicode(inst.__str__())
            return self.lastError
    

    
    def uploadFileToCloud(self,filepath, name, mimetype="", md5=None, replace=True, progressCallback=None, chunkSize=CHUNK_SIZE, concurrency=UPLOAD_CONCURRENCY, skipUpload=False):
        #name can contain the a "folderlike" prefix like "folder1/folder2/file.txt"
        
        logFolder="/var/log/JefeCore/CloudUploads"
        try:
            os.makedirs(logFolder)
        except Exception, inst:
            pass
        logFilename=os.path.join(logFolder, os.path.basename(filepath)+".log")
        log = open(logFilename,'a')               
        
        result=""
        
        if self.initialize(self.bucketName) != 0:
            return self.lastError

        print("Uploading file (multipart): "+filepath+" to Bucket "+self.bucketName+"://"+name)
        try:
            if filepath != "":
                theKey=Key(self.bucket)

                theKey.key=name
                
                #if mimetype:
                #        theKey.set_metadata("Content-Type", mimetype)
                
                start=time.time()
                size = os.stat(filepath).st_size
                if True or (size > (chunkSize*2) and concurrency > 1):
                    #pool = gevent.pool.Pool(concurrency)
                    mp = self.bucket.initiate_multipart_upload(name, reduced_redundancy=True)
         
                    greenlets = []
                    idx = offset = 0
                    totalParts = size/chunkSize
                    #logFolder=os.path.join("/tmp/cloudUploadLogs/",os.path.basename(filepath))
                    
                    pool=Pool(concurrency)
                    results=[]
                    while offset < size:
                        #greenlets.append( pool.spawn(upload_part, mp, filepath, idx, offset, totalParts, chunkSize,
                        #                             self.AWSS3_API_KEYID, self.AWSS3_API_ACCESSKEY, logFile=log, skipUpload=skipUpload) )
                        
                        results.append(
                            pool.apply_async(upload_part,
                                         [mp, filepath, idx, offset, totalParts, chunkSize,
                                                     self.AWSS3_API_KEYID, self.AWSS3_API_ACCESSKEY, log, skipUpload])
                            )
                        idx += 1
                        offset += chunkSize
         
                    #gevent.joinall(greenlets)
                    pool.close()
                    pool.join()
                    
                    cancel_due_to_errors=False
                    for r in results:
                        try:
                            r.get(1)
                        except Exception, inst:
                            self.lastErrorString+=inst.__str__()+"\n"
                            self.lastError=1
                            cancel_due_to_errors=False
                            line="CANCELING UPLOAD DUE TO ERRORS: %s\n"%(os.path.basename(filepath))
                            print line
                            log.write(line)
                            log.flush()
                            
                    if cancel_due_to_errors:
                        cmp=mp.cancel_upload()
                    
                    line="\n%s FINISHED\n"%(os.path.basename(filepath))
                    print line
                    log.write(line)
                    log.flush()
                    
                    try:
                        os.rename(logFilename,logFilename+".done")
                    except Exception, inst:
                        pass
                    
                    if not skipUpload and self.lastError==0:
                        cmp = mp.complete_upload()
                        self.upload_elapsed = time.time() - start
                        size = float(size)/1024/1024
                        avgSpeed=size/self.upload_elapsed
                        line="Elapsed Time: %i:%i\nTotal Size: %.2fMB\nAverage Speed: %fMB/s"%(self.upload_elapsed/60, self.upload_elapsed % 60, size, avgSpeed)
                        print line
                        log.write(line)
                        log.flush()
                        
                    
                    
                else:
                    f = open(filepath)
                    theKey=Key(self.bucket)
                    theKey.key=name
                    if mimetype:
                            theKey.set_metadata("Content-Type", mimetype)
                    theKey.set_contents_from_file(f, reduced_redundancy=True, replace=True)
                    f.close()
         
                
                            
                log.close()
                return self.lastError
            
        except Exception, inst:
            self.lastError=2
            self.lastErrorString="Error while uploading file %s (%s) to bucket %s: "%(filepath, name, self.bucket.name )+unicode(inst.__str__())
            return self.lastError
    
   
    def uploadFileToCloudOLD(self,filepath, name, mimetype="", md5=None, replace=True, progressCallback=None):
        #name can contain the a "folderlike" prefix like "folder1/folder2/file.txt"
        result=""
         #just debugging, to facilitate test don't upload to cloud.
        #self.lastError=0
        #self.lastErrorString=""
        #return self.lastError
    
        if self.initialize(self.bucketName) != 0:
            return self.lastError
        
        logger.info("Uploading file: "+filepath+" to Bucket "+self.bucketName+"://"+name)
        try:
            if filepath != "":
                theKey=Key(self.bucket)

                theKey.key=name
                if mimetype:
                        theKey.set_metadata("Content-Type", mimetype)
                
                theKey.set_contents_from_filename(filepath, replace=replace, cb=progressCallback, md5=md5)

                self.lastError=0
                self.lastErrorString=""
                return self.lastError
            
        except Exception, inst:
            self.lastError=2
            self.lastErrorString="Error while uploading file %s (%s) to bucket %s: "%(filepath, name, self.bucket.name )+unicode(inst.__str__())
            return self.lastError
            
    def deleteFileFromCloud(self,name):
        result=""
                
        if self.initialize(self.bucketName) != 0:
            self.lastErrorString=u"Error initializing bucket %s"%(self.bucketName)
            return self.lastError

        try:

            if name != "":
                theKey=Key(self.bucket)
                theKey.key=name
                self.bucket.delete_key(theKey)
                #logger.info(u"DELETED CLOUDFILE: "+name)
                self.lastError=0
                self.lastErrorString=""
                return self.lastError
                
            else:
                self.lastError=1
                logger.error(u"COULD NOT DELETE CLOUDFILE: No name provided for key")
                self.lastErrorString="No name provided for key"
                return self.lastError
        
        except Exception as inst:
            self.lastError=2
            logger.error("Exception Ocurred in deleteFileFromCloud")
            self.lastErrorString=u"Error while deleting file %s/%s: %s"%(self.bucketName,name,unicode(inst.__str__()))
            logger.error(self.lastErrorString)
            return self.lastError
        pass


def safe_b64encode(data):
    safe = ['+-', '=_', '/~']
    data = base64.b64encode(data)
    for s, r in safe:
        data = data.replace(s, r)
    return data

def rsa_sha1_sign(data, keyfile=None,):
    key = RSA.load_key(keyfile)
    #print key.check_key()
    return key.sign(hashlib.sha1(data).digest(), 'sha1')
    
def sign_restricted_url(baseURL, filename, sourceIP, cachable=False, cloudKeyPairID="SOMEKEYPAIRID", cloudKeyFile="", ignoreIP=False):
    
    #There is a tradeoff between signed variable expiration times and cache.
    #If each request gets a different expiration (i.e, a different signature) then browser will never cache.
    
    #Two options,
    #   1. Bypass cache and change expiration on each request
    #   2. Have a "fixed" expiration period. Maybe every hour on the hour. That way, all requests to an asset within one hour will have the same signature and thus cached in that hour.
    #       a. Get current time and round up to the nearest hour, make that the expiration time.
    
    resource = baseURL+filename
    #expiresIn = getattr(settings, "CLOUDFRONT_EXPIRES_IN", 60*60*3) # 3h
    #now=int(time.time())
    #logger.info("Now: "+str(now))
    #expirationTime = now+expiresIn
    #expirationTime = now+30
    
    now = datetime.datetime.now()
    
    #logger.info("Now: "+now.isoformat())
    
    #The expiration for links is when the current hour finishes, so if they request something at 3:45:26 PM the link will be valid until 4:00:00 PM, the same as if it was requested at 3:59:59 PM.
    #Why do this instead of just expiring one hour from now? Because if we change the signature every request, neither the browser, proxy or servers will cache the response. This gives us a good balance
    #of caching and expiration security.
    #now+=datetime.timedelta(minutes=(60-now.minute), seconds=(-now.second),microseconds=-(now.microsecond)) #round the microconds down to zero, round the seconds up to the next minute
    now+=datetime.timedelta(hours=24) #round the microconds down to zero, round the seconds up to the next minute
    #logger.info("Now rounded up: "+now.isoformat())
    expirationTime=int(time.mktime(now.timetuple()))
    
    if USE_SOURCEIP_TO_SIGN_CLOUD==False:
        sourceIP=CLOUD_SIGNATURE_IP
    
    #print("Signing resource %s with IP %s . "%(resource,sourceIP))
    #make a custom policy, restricting to source IP
    
    if ignoreIP == False: 
        policy={
                "Statement": [{ 
                   "Resource":resource, 
                   "Condition":{ 
                      "IpAddress":{"AWS:SourceIp":sourceIP+"/32"}, 
                      "DateLessThan":{"AWS:EpochTime":expirationTime}
                        }
                    }]
                }
    else:
        policy={
                "Statement": [{ 
                   "Resource":resource, 
                   "Condition":{  
                      "DateLessThan":{"AWS:EpochTime":expirationTime},
                        }
                    }]
                }

    policy = json.dumps(policy, separators=(',', ':'))
    #print policy
    encodedPolicy=safe_b64encode(policy)
    signature = safe_b64encode(rsa_sha1_sign(policy, cloudKeyFile))
    return "%s?Policy=%s&Signature=%s&Key-Pair-Id=%s" % (resource,
                                                         encodedPolicy,
                                                         signature,
                                                         cloudKeyPairID)
 
