This is some of the work I've been doing on python (and I'm permitted to show).

CloudStuff:  Used in production at OllinVFX (with permission, sensitive information is redacted). Consists of several scripts that keep watch over a folder in which cloudjobs will be created as needed by our Core Server (not provided), to upload new files to an Amazon S3 instance.

KeepassDrive (TODO):  I use [Keepass](http://keepass.info/ )  to store all my passwords with a master key. I keep a copy of the database on my google drive, so each time I need to look up a password and I'm not at a trusted computer I download the file, download keepass, unlock the DB and delete everything afterwards. If I need to alter the DB (add , modify or remove a password) then the process also has to save the DB and reupload it manually to my Drive. So this little program is a (VERY INSECURE, PLEASE DON'T USE IT AS A SERIOUS TOOL) proof of concept of automating this process. The script downloads Keepass, uncompresses it, downloads the DB file from Google Drive, and runs it. Once the user exits Keepass, the DB file is reuploaded (if something changed) and everything is deleted afterwards.


